package com.android.thetyras.ckeyboard;

import android.app.Dialog;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.IBinder;
import android.text.method.MetaKeyKeyListener;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import java.util.List;
/**
 *  Phase 1 implementation of Developer keyboard for C#
 */
public class CSharpKeys extends InputMethodService implements KeyboardView.OnKeyboardActionListener
{
    private InputMethodManager mInputMethodManager;
    private KeyboardView mInputView;

    private StringBuilder mComposing = new StringBuilder();
    private int mLastDisplayWidth;
    private boolean mShifted;
    private long mMetaState;
    private boolean light = true;

    private ExtKeyboard mSymbolsKeyboard;
    private ExtKeyboard mCodeKeyboard;
    private ExtKeyboard mQwertyKeyboard;

    private ExtKeyboard mCurKeyboard;

    private String mWordSeparators;

    /**
     * Main initialization of the input method component.
     */
    @Override public void onCreate() {
        super.onCreate();
        mInputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        mWordSeparators = getResources().getString(R.string.word_separators);
    }

    /**
     * UI initialization
     */
    @Override public void onInitializeInterface()
    {
        if (mQwertyKeyboard != null)
        {
            // Configuration changes can happen after the keyboard gets recreated,
            // must be able to re-build the keyboards if the available space has changed.
            int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }
        //assign xml files to respective keyboard variables
        mQwertyKeyboard = new ExtKeyboard(this, R.xml.qwerty);
        mSymbolsKeyboard = new ExtKeyboard(this, R.xml.symbols);
        mCodeKeyboard = new ExtKeyboard(this, R.xml.code_1);
    }

    /**
     * Called by the framework when input method is to be displayed
     */
    @Override public View onCreateInputView()
    {
        mShifted = false;
        // *** theme is not fully implemented currently
        if(light == true)
        {
            this.mInputView = (KeyboardView) this.getLayoutInflater().inflate(R.layout.light_input, null);
        }
        else
        {
            this.mInputView = (KeyboardView) this.getLayoutInflater().inflate(R.layout.dark_input, null);
        }
        mInputView.setOnKeyboardActionListener(this);
        setCSharpKeyboard(mQwertyKeyboard);
        mInputView.setPreviewEnabled(false);
        return mInputView;
    }

    /*
     *Set the current keyboard to a specified keyboard
     */
    private void setCSharpKeyboard(ExtKeyboard newKeyboard)
    {
        mInputView.setKeyboard(newKeyboard);
    }

    /**
     * called when the user is done editing a field.
     */
    @Override public void onFinishInput()
    {
        super.onFinishInput();
        // Clear current composing text and candidates.
        mComposing.setLength(0);
        mCurKeyboard = mQwertyKeyboard;
        if (mInputView != null) {
            mInputView.closing();
        }
    }

    @Override public void onStartInputView(EditorInfo attribute, boolean restarting)
    {
        super.onStartInputView(attribute, restarting);
        // Apply the selected keyboard to the input view.
        setCSharpKeyboard(mCurKeyboard);
        mInputView.closing();
        final InputMethodSubtype subtype = mInputMethodManager.getCurrentInputMethodSubtype();
    }

    @Override
    public void onCurrentInputMethodSubtypeChanged(InputMethodSubtype subtype)
    {
        //mInputView.setSubtypeOnSpaceKey(subtype);
    }

    /**
     * Deal with the editor reporting movement of its cursor.
     */
    @Override public void onUpdateSelection(int oldSelStart, int oldSelEnd,
                                            int newSelStart, int newSelEnd,
                                            int candidatesStart, int candidatesEnd)
    {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
                candidatesStart, candidatesEnd);

        // If the current selection in the text view changes, we should
        // clear whatever candidate text we have.
        if (mComposing.length() > 0 && (newSelStart != candidatesEnd || newSelEnd != candidatesEnd))
        {
            mComposing.setLength(0);
            InputConnection ic = getCurrentInputConnection();
            if (ic != null)
            {
                ic.finishComposingText();
            }
        }
    }

    /**
     * translates incoming hard key events in to edit operations on an
     * InputConnection.
     */
    private boolean translateKeyDown(int keyCode, KeyEvent event)
    {
        mMetaState = MetaKeyKeyListener.handleKeyDown(mMetaState, keyCode, event);
        int c = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(mMetaState));
        mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(mMetaState);
        InputConnection ic = getCurrentInputConnection();
        if (c == 0 || ic == null)
        {
            return false;
        }
        boolean dead = false;
        if ((c & KeyCharacterMap.COMBINING_ACCENT) != 0)
        {
            dead = true;
            c = c & KeyCharacterMap.COMBINING_ACCENT_MASK;
        }
        if (mComposing.length() > 0)
        {
            char accent = mComposing.charAt(mComposing.length() -1 );
            int composed = KeyEvent.getDeadChar(accent, c);
            if (composed != 0)
            {
                c = composed;
                mComposing.setLength(mComposing.length()-1);
            }
        }
        onKey(c, null);
        return true;
    }

    /**
     * monitor key events being delivered to the application.
     */
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        switch (keyCode)
        {
            case KeyEvent.KEYCODE_BACK:
                // The InputMethodService already takes care of the back
                // key for us, to dismiss the input method if it is shown.
                // However, our keyboard could be showing a pop-up window
                // that back should dismiss, so we first allow it to do that.
                if (event.getRepeatCount() == 0 && mInputView != null)
                {
                    if (mInputView.handleBack())
                    {
                        return true;
                    }
                }
                break;
            case KeyEvent.KEYCODE_DEL:
                // Special handling of the delete key: if we currently are
                // composing text for the user, we want to modify that instead
                // of let the application to the delete itself.
                if (mComposing.length() > 0)
                {
                    onKey(Keyboard.KEYCODE_DELETE, null);
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_ENTER:
                // Let the underlying text editor always handle these.
                return false;
            default:
                // For all other keys, if we want to do transformations on
                // text being entered with a hard keyboard, we need to process
                // it and do the appropriate action.
                //if (PROCESS_HARD_KEYS)
                //{
                // no necessary implementation of hard keyboard use at this point
               //}
        }
        return super.onKeyDown(keyCode, event);
    }
    /**
     * monitor key events being delivered to the application.
     */
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Helper function to commit any text being composed in to the editor.
     */
    private void commitTyped(InputConnection inputConnection)
    {
        if (mComposing.length() > 0)
        {
            inputConnection.commitText(mComposing, mComposing.length());
            mComposing.setLength(0);
        }
    }

    /**
     * Helper to update the shift state of our keyboard based on the initial
     * editor state.
     */


    /**
     * Helper to determine if a given character code is alphabetic.
     */
    private boolean isAlphabet(int code)
    {
        if (Character.isLetter(code))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Helper to send a key down / key up pair to the current editor.
     */
    private void keyDownUp(int keyEventCode)
    {
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    }

    /**
     * Helper to send a character to the editor as raw key events.
     */
    private void sendKey(int keyCode)
    {
        switch (keyCode)
        {
            case '\n':
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            default:
                if (keyCode >= '0' && keyCode <= '9')
                {
                    keyDownUp(keyCode - '0' + KeyEvent.KEYCODE_0);
                }
                else
                {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                }
                break;
        }
    }
    // Implementation of KeyboardViewListener
    public void onKey(int primaryCode, int[] keyCodes) {
        if (isWordSeparator(primaryCode))
        {
            // Handle separator
            if (mComposing.length() > 0)
            {
                commitTyped(getCurrentInputConnection());
            }
            sendKey(primaryCode);
        }
        else if (primaryCode == Keyboard.KEYCODE_DELETE)
        {
            handleBackspace();
        }
        else if (primaryCode == Keyboard.KEYCODE_SHIFT)
        {
            setToShifted(!mShifted);
            mInputView.invalidate();
        }
        else if (primaryCode == Keyboard.KEYCODE_CANCEL)
        {
            handleClose();
            return;
        }
        else
        {
            handleCharacter(primaryCode, keyCodes);
        }
    }
    public void onText(CharSequence text)
    {
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;
        ic.beginBatchEdit();
        if (mComposing.length() > 0)
        {
            commitTyped(ic);
        }
        ic.commitText(text, 0);
        ic.endBatchEdit();
    }

    private void handleBackspace()
    {
        final int length = mComposing.length();
        if (length > 1)
        {
            mComposing.delete(length - 1, length);
            getCurrentInputConnection().setComposingText(mComposing, 1);
        }
        else if (length > 0)
        {
            mComposing.setLength(0);
            getCurrentInputConnection().commitText("", 0);
        }
        else
        {
            keyDownUp(KeyEvent.KEYCODE_DEL);
        }
    }

    private void setToShifted(boolean isShift)
    {
        List<Keyboard.Key> mKeys = mInputView.getKeyboard().getKeys();
        for(int i = 0; i < mKeys.size(); i++)
        {
            Keyboard.Key element = mKeys.get(i);
            if(isAlphabet(element.codes[0]) && element.label.toString().length() == 1)
            {
                if (isShift)
                {
                    element.label = element.label.toString().toUpperCase();
                    //element.codes[0] = element.codes[0] - 32;

                }
                else
                {
                    element.label = element.label.toString().toLowerCase();
                    //element.codes[0] = element.codes[0] + 32;
                }
            }
        }
        mShifted = !mShifted;
        mInputView.setKeyboard(mQwertyKeyboard);
    }

    private void handleCharacter(int primaryCode, int[] keyCodes)
    {
        if (isInputViewShown())
        {
            if (mInputView.isShifted())
            {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        if (isAlphabet(primaryCode))
        {
            mComposing.append((char) primaryCode);
            getCurrentInputConnection().setComposingText(mComposing, 1);
            //updateShiftKeyState(getCurrentInputEditorInfo());
        }
        else
        {
            switch (primaryCode)
            {
                case -898:
                    mInputView.setKeyboard(mSymbolsKeyboard);
                    break;
                case -899:
                    mInputView.setKeyboard(mQwertyKeyboard);
                    break;
                case -900:
                    mInputView.setKeyboard(mCodeKeyboard);
                    break;
                case -902:
                    getCurrentInputConnection().commitText("public ", 1);
                    break;
                case -903:
                    getCurrentInputConnection().commitText("private ", 1);
                    break;
                case -904:
                    getCurrentInputConnection().commitText("void ", 1);
                    break;
                case -905:
                    getCurrentInputConnection().commitText("return  ", 1);
                    break;
                case -906:
                    getCurrentInputConnection().commitText("true", 1);
                    break;
                case -907:
                    getCurrentInputConnection().commitText("false", 1);
                    break;
                case -908:
                    getCurrentInputConnection().commitText("//", 1);
                    break;
                case -909:
                    getCurrentInputConnection().commitText("/**/", 1);
                    getCurrentInputConnection().commitText("", -2);
                    break;
                case -910:
                    getCurrentInputConnection().commitText("if()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -911:
                    getCurrentInputConnection().commitText("else ", 1);
                    break;
                case -912:
                    getCurrentInputConnection().commitText("while()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -913:
                    getCurrentInputConnection().commitText("do ", 1);
                    break;
                case -914:
                    getCurrentInputConnection().commitText("for()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -915:
                    getCurrentInputConnection().commitText("foreach()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -916:
                    getCurrentInputConnection().commitText("switch()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -917:
                    getCurrentInputConnection().commitText("case ", 1);
                    break;
                case -918:
                    getCurrentInputConnection().commitText("this ", 1);
                    break;
                case -919:
                    getCurrentInputConnection().commitText("using()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -920:
                    getCurrentInputConnection().commitText("new ", 1);
                    break;
                case -921:
                    getCurrentInputConnection().commitText("object ", 1);
                    break;
                case -922:
                    getCurrentInputConnection().commitText("in ", 1);
                    break;
                case -923:
                    getCurrentInputConnection().commitText("out ", 1);
                    break;
                case -924:
                    getCurrentInputConnection().commitText("break ", 1);
                    break;
                case -925:
                    getCurrentInputConnection().commitText("default ", 1);
                    break;
                case -926:
                    getCurrentInputConnection().commitText("try ", 1);
                    break;
                case -927:
                    getCurrentInputConnection().commitText("catch()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -928:
                    getCurrentInputConnection().commitText("finally ", 1);
                    break;
                case -929:
                    getCurrentInputConnection().commitText("throw ", 1);
                    break;
                case -930:
                    getCurrentInputConnection().commitText("ref ", 1);
                    break;
                case -931:
                    getCurrentInputConnection().commitText("typeof()", 1);
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -932:
                    getCurrentInputConnection().commitText("const ", 1);
                    break;
                case -933:
                    getCurrentInputConnection().commitText("int ", 1);
                    break;
                case -934:
                    getCurrentInputConnection().commitText("bool ", 1);
                    break;
                case -935:
                    getCurrentInputConnection().commitText("double ", 1);
                    break;
                case -936:
                    getCurrentInputConnection().commitText("string ", 1);
                    break;
                case -937:
                    getCurrentInputConnection().commitText("char ", 1);
                    break;
                case -938:
                    getCurrentInputConnection().commitText("long ", 1);
                    break;
                case -939:
                    getCurrentInputConnection().commitText("var ", 1);
                    break;
                case -940:
                    getCurrentInputConnection().commitText("null", 1);
                    break;
                case -941:
                    getCurrentInputConnection().commitText("==  ", 1);
                    break;
                case -942:
                    getCurrentInputConnection().commitText("!= ", 1);
                    break;
                case -943:
                    getCurrentInputConnection().commitText(">= ", 1);
                    break;
                case -944:
                    getCurrentInputConnection().commitText("<= ", 1);
                    break;
                case -945:
                    getCurrentInputConnection().commitText("?? ", 1);
                    break;
                case -946:
                    getCurrentInputConnection().commitText("&& ", 1);
                    break;
                case -947:
                    getCurrentInputConnection().commitText("|| ", 1);
                    break;
                case -948:
                    getCurrentInputConnection().commitText("++ ", 1);
                    break;
                case -949:
                    getCurrentInputConnection().commitText("", -1);
                    break;
                case -950:
                    getCurrentInputConnection().commitText("", 2);
                    break;
                case Keyboard.KEYCODE_DONE:

                    break;
                default:
                    char code = (char) primaryCode;
                    if (Character.isLetter(code) && mShifted)
                    {
                        code = Character.toUpperCase(code);
                    }
                    getCurrentInputConnection().commitText(String.valueOf(code), 1);
            }
        }
    }
    private void handleClose()
    {
        commitTyped(getCurrentInputConnection());
        requestHideSelf(0);
        mInputView.closing();
    }
    private IBinder getToken()
    {
        final Dialog dialog = getWindow();
        if (dialog == null)
        {
            return null;
        }
        final Window window = dialog.getWindow();
        if (window == null)
        {
            return null;
        }
        return window.getAttributes().token;
    }
    private void handleLanguageSwitch()
    {
        mInputMethodManager.switchToNextInputMethod(getToken(), false /* onlyCurrentIme */);
    }

    private String getWordSeparators()
    {
        return mWordSeparators;
    }

    public boolean isWordSeparator(int code)
    {
        String separators = getWordSeparators();
        return separators.contains(String.valueOf((char)code));
    }

    public void swipeRight()
    {

    }

    public void swipeLeft()
    {
        handleBackspace();
    }
    public void swipeDown()
    {
        handleClose();
    }
    public void swipeUp()
    {
        //required for implementation
    }

    public void onPress(int primaryCode)
    {
        mInputView.setPreviewEnabled(false);
    }

    public void onRelease(int primaryCode)
    {
        //required for implementation
    }
}